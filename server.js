const express = require('express');
const app = express()
require("dotenv").config()



const port = process.env.PORT || 8080 

//create the server 
app.listen (port, () => console.log (`Server is running on ${port}`))


app.get ("/myProject", (req, res) => {
    res.sendFile(`${__dirname}/src/index.htm`)
})

app.get ("*", (req, res) => {
    res.send ("Page not found!!!")
})